import sys
from funciones import *

opcion = 1
while opcion:
    print """
        [1] Cargar grafo
        [2] Mostrar el grafo
        [3] Mostrar numero de aristas y grados de los vertices
        [4] Mostrar las componentes conexas del grafo
        [5] Mostrar la componente conexa con mayor numero de vertices (G1)
        [6] Hallar el arbol generador de G1
        [7] Hallar un circuito euleriano en caso de que G1 sea un grafo euleriano
        [8] Hallar un ciclo hamiltoniano en caso de que G1 sea un grafo hamiltoniano
        [0] Salir\n"""
    opcion = input("Opcion: ")

    if opcion == 1:
        print "Introduce el nombre del fichero que contiene la matriz."
        print "Nota: El fichero debe estar en la carpeta matrices"
        ruta = raw_input("Nombre del fichero: ")
        g = CargarGrafo("matrices/" + ruta)

    elif opcion == 2:
        print "Mostrando grafo..."
        MostrarGrafo(g)

    elif opcion == 3:
        MostrarInformacion(g)

    elif opcion == 4: 
        print "Mostrando componentes conexas..."
        MostrarComponentesConexas(g)

    elif opcion == 5:
        print "Mostrando la componente connexa con mayor numero de vertices..."
        mayor_componente_conexa = ObtenerComponenteConexaMayor(g)
        MostrarGrafo(mayor_componente_conexa)

    elif opcion == 6:
        print "Mostrando arbol generador de G1..."
        mayor_componente_conexa = ObtenerComponenteConexaMayor(g)
        arbol_generador_grafo = ObtenerArbolGenerador(mayor_componente_conexa)
        MostrarGrafo(arbol_generador_grafo)

    elif opcion == 7:
        mayor_componente_conexa = ObtenerComponenteConexaMayor(g)
        camino_euleriano = ObtenerCaminoEuleriano(mayor_componente_conexa)
        if camino_euleriano:
            print "G1 es un grafo euleriano!"
            print "Un camino euleriano es ... " + str(camino_euleriano)
        else:
            print "G1 no es euleriano"

    elif opcion == 8:
        mayor_componente_conexa = ObtenerComponenteConexaMayor(g)
        camino_hamiltoniano = ObtenerCaminoHamiltoniano(mayor_componente_conexa)
        if camino_hamiltoniano:
            print "G1 es un grafo hamiltoniano!"
            print "Un ciclo hamiltoniano es ... " + str(camino_hamiltoniano)
        else:
            print "G1 no es hamiltoniano"

print "Hasta otra!"
