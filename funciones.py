from sage.all import *
import numpy

def CargarGrafo(rutaFichero):
    matrizNumpy = numpy.loadtxt(rutaFichero, dtype=int)
    matrizSage  = matrix(matrizNumpy)
    grafo       = Graph(matrizSage)
    return grafo

def MostrarInformacion(grafo):
    print "Grados de los vertices... "  + str(grafo.degree())
    print "Numero de aristas = "        + str(grafo.size())

def MostrarGrafo(grafo):
    grafo.show('js')

def ObtenerComponentesConexas(grafo):
    return grafo.connected_components()

def MostrarComponentesConexas(grafo):
    for componente_conexa in grafo.connected_components():
        grafo.subgraph(componente_conexa).show()

def ObtenerComponenteConexaMayor(grafo):
    max_componente_conexa = []
    for componente_conexa in grafo.connected_components():
        if len(componente_conexa) > len(max_componente_conexa):
            max_componente_conexa = componente_conexa 

    max_componente_conexa_grafo = grafo.subgraph(max_componente_conexa)
    return max_componente_conexa_grafo

def ObtenerArbolGenerador(grafo):
    arbol = grafo.random_spanning_tree()
    grafo_arbol = Graph(arbol)
    return grafo_arbol

def ObtenerCaminoEuleriano(grafo):
    return grafo.eulerian_circuit(labels=False, path=True)

def ObtenerCaminoHamiltoniano(grafo):
    exito, ciclo_hamiltoniano = grafo.hamiltonian_cycle(algorithm='backtrack')
    return ciclo_hamiltoniano if exito else None
